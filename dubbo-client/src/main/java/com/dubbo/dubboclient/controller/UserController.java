package com.dubbo.dubboclient.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.dubbo.config.annotation.Reference;
import com.dubbo.dubboapi.UserService;

@RestController
public class UserController {

	@Reference
	UserService userService;

	@RequestMapping(value = "login", method = RequestMethod.POST)
	@ResponseBody
	public String login() {
		String str = userService.getUserName();
		System.out.println(str);
		return str;
	}

}
