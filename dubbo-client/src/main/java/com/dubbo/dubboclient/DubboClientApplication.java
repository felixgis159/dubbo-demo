package com.dubbo.dubboclient;

import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class DubboClientApplication implements CommandLineRunner, DisposableBean {
	private static final Logger logger = LoggerFactory.getLogger(DubboClientApplication.class);

	private final static CountDownLatch latch = new CountDownLatch(1);
	private static ConfigurableApplicationContext context;

	public static void main(String[] args) throws InterruptedException {
		context = SpringApplication.run(DubboClientApplication.class, args);
		latch.await();
	}

	@Override
	public void run(String... args) throws Exception {
		logger.info("服务消费者启动完毕------>>启动完毕");
	}

	@Override
	public void destroy() throws Exception {
		latch.countDown();
		context.close();
		logger.info("服务消费者关闭------>>服务关闭");
	}
}
